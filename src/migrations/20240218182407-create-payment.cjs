'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Payments', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            userId: {
                type: Sequelize.INTEGER
            },
            planId: {
                type: Sequelize.INTEGER
            },
            amount: {
                type: Sequelize.FLOAT
            },
            authority: {
                type: Sequelize.STRING
            },
            gateway: {
                type: Sequelize.STRING ,
            },
            status: {
                type: Sequelize.BOOLEAN ,
                default : 0
            },
            refId: {
                type: Sequelize.STRING ,
                allowNull : true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Payments');
    }
};