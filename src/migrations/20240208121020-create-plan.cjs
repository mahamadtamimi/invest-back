'use strict';
const {DataTypes} = require("sequelize");
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Plans', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING
            },
            minimumDeposit: {
                type: Sequelize.INTEGER
            },

            dailyCredit: {
                type: Sequelize.INTEGER
            },
            withdrawalPeriod: {
                type: Sequelize.INTEGER
            },

            interestRates: {
                type: Sequelize.FLOAT
            },
            description : {
                type: Sequelize.TEXT
            },
            active: {
                type: DataTypes.BOOLEAN, default: 1
            },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Plans');
    }
};