import express from "express";
import {sendTestEmail} from "../controllers/email.controller.js";



const emailRoute = express.Router()


emailRoute.get('/test', sendTestEmail)


export {emailRoute}