import express from "express";
import {
    list,
    login,
    signup,
    fieldResponse,
    userPlan,
    userTransactions,
    getMe,
    userActivePlan,
    userWallets,
    createNewUser,
    getUserInfo,
    updateUserInfo,
    updateUserWalletAddress,
    checkGoogleVerificate, disbaleGoogleVerificate
} from "../controllers/Users.controller.js";
import {changePasswordFromDashboard, twoVerification, twoVerificationVerify} from "../controllers/Auth.controller.js";



const userRoute = express.Router()



userRoute.get('/auth/me', getMe)
userRoute.post('/auth/register', signup)
userRoute.post('/auth/login', login)
userRoute.post('/auth/change-user-password', changePasswordFromDashboard)
userRoute.get('/auth/two-verification', twoVerification)
userRoute.post('/auth/two-verification-verify', twoVerificationVerify)
userRoute.post('/auth/verify-google-code', checkGoogleVerificate)
userRoute.post('/auth/disable-google-code', disbaleGoogleVerificate)

userRoute.get('/users/list', list)
userRoute.get('/user/plan', userPlan)
userRoute.get('/user/transactions', userTransactions)
userRoute.get('/user-active-plans', userActivePlan)
userRoute.get('/user-wallet', userWallets)


userRoute.post('/create-new-user', createNewUser)


userRoute.post('/get-user-info' , getUserInfo)
userRoute.post('/update-user-wallet' , updateUserInfo)
userRoute.post('/update-user-wallet-address' , updateUserWalletAddress)

export {userRoute}


