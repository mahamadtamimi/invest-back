import express from "express";

import {balance, getPaymentLink, verify} from "../controllers/Payment.controller.js";



const paymentRoute = express.Router()




paymentRoute.post('/get-payments-link', getPaymentLink)
paymentRoute.post('/verify', verify)


paymentRoute.get('/balance' , balance)


export {paymentRoute}