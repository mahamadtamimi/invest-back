import express from "express";
import {
    adminTicketList,
    answerTicket, closeTickets, closeTicketsFromAdmin,
    newTicket, userTicketArchiveList,
    userTicketList
} from "../controllers/Ticket.controller.js";


const ticketRoute = express.Router()


ticketRoute.post("/new/ticket", newTicket)
ticketRoute.get("/user/tickets", userTicketList)
ticketRoute.get("/user/tickets-archive", userTicketArchiveList)


ticketRoute.get("/admin/tickets", adminTicketList)

ticketRoute.post("/admin/tickets/answer", answerTicket)
ticketRoute.post("/close-tickets", closeTickets)
ticketRoute.post("/admin/close-tickets", closeTicketsFromAdmin)

export {ticketRoute}