import express from "express";
import {
    changePassword,
    forgotPassword,
    forgotPasswordGetCode,
    getCode,
    verifyCode
} from "../controllers/Auth.controller.js";

const authRoute = express.Router()

authRoute.get('/auth/get-code', getCode)
authRoute.post('/auth/verify-code', verifyCode)
authRoute.post('/auth/forgot-password', forgotPassword)
authRoute.post('/auth/forgot-password/new-code', forgotPasswordGetCode)
authRoute.post('/auth/change-password', changePassword)

export {authRoute}
