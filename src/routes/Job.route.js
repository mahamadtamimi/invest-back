import express from "express";
import {run, run2, run3, run4} from '../controllers/job.controller.js'


const jobRoute = express.Router()


jobRoute.get('/run', run)

jobRoute.get('/run2', run2)

jobRoute.get('/run3', run3)
jobRoute.get('/run4', run4)

export {jobRoute}