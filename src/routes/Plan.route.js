import express from "express";

import {
    changeActivate,
    destroyPlan,
    planList, planListAll,
    planStore,
    showPlan,
    updatePlan
} from "../controllers/Plan.controller.js";

import {storePlanValidator} from "../middlewares/validators/Plan.validator.js";


const planRoute = express.Router()


planRoute.get('/plans/list', planList)
planRoute.get('/plans/list-all', planListAll)
planRoute.post('/plan/store', planStore)
planRoute.post('/plan/show', showPlan)
planRoute.post('/plan/update', updatePlan)
planRoute.post('/plan/change-activate', changeActivate)



export {planRoute}