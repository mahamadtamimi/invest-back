import express from "express";

import {seeder} from "../controllers/Seeder.controller.js"

const SeedRoute = express.Router()


SeedRoute.get('/seed/start', seeder)



export {SeedRoute}