import express from "express";
import {
    getWalletAddress,
    me, payout,
    updateWalletAddress, validateWalletAddress,
    withdrawal, withdrawalComplete,
    withdrawalList
} from "../controllers/Wallet.controller.js";



const walletRoute = express.Router()


walletRoute.get('/wallet/me', me)

walletRoute.post('/wallet/withdrawal', withdrawal)

walletRoute.post('/wallet/update-wallet-address', updateWalletAddress)
walletRoute.get('/wallet/get_wallet_address', getWalletAddress)


walletRoute.get('/withdrawal/list', withdrawalList )
walletRoute.post('/withdrawal/set-complete', withdrawalComplete )


walletRoute.post('/wallet/validate-address' , validateWalletAddress)
walletRoute.post('/wallet/payout' , payout)


export {walletRoute}


