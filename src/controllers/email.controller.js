import nodemailer from "nodemailer";
import * as path from "node:path";
import hbs from 'nodemailer-express-handlebars';
import 'dotenv/config'


export const sendTestEmail = [
    sendNewEmail,
]


function sendNewEmail(req, res) {
    main().catch(console.error);
    return res.status(200).send({success: true})

}



const transporter = nodemailer.createTransport({
    service: "Gmail",
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user:  process.env.EMAIL,
        pass: process.env.EMAILPASSWORD,
    },
});

const adminTransporter = nodemailer.createTransport({
    service: "Gmail",
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user:  process.env.EMAIL,
        pass: process.env.EMAILPASSWORD,
    },
});




const handlebarOptions = {
    viewEngine: {
        partialsDir: path.resolve('./src/server/'),
        defaultLayout: false,
    },
    viewPath: path.resolve('./src/server/'),
};


// use a template file with nodemailer
transporter.use('compile', hbs(handlebarOptions))


export async function main(code,email) {
    const mailOptions = {
        from: 'investitionhub.com', // sender address
        template: "email", // the name of the template file, i.e., email.handlebars
        to: email, // list of receivers
        subject: `investitionhub.com`,
        context: {
            code: code,
            company: 'my company'
        },
    };


    try {
        await transporter.sendMail(mailOptions);
    } catch (error) {
        console.log(`Nodemailer error sending email to `, error);
    }

}



export async function AdminEmail(massage) {
    const mailOptions = {
        from: 'investitionhub.com', // sender address
        template: "email", // the name of the template file, i.e., email.handlebars
        to: process.env.SUPPORTEMAIL, // list of receivers
        subject: `investitionhub.com`,
        text: massage
    };


    try {
        await adminTransporter.sendMail(mailOptions);
    } catch (error) {
        console.log(`Nodemailer error sending email to `, error);
    }

}


