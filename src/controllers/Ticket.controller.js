import {Ticket, TicketBody, User} from "../models/index.js";
import user from "../models/user.js";
import passport from "passport";

export const userTicketArchiveList =[
    passport.authenticate('getMe', {session: false})
    , getArchiveList
]
export const closeTickets = [
    passport.authenticate('getMe', {session: false}),
    setTicketStatusClose
]

export const newTicket = [createTicket]

export const userTicketList = [
    passport.authenticate('getMe', {session: false})
    , getUserTickets
]

export const adminTicketList = [
    passport.authenticate('isSuperAdmin', {session: false})
    , getAllTickets
]

export const answerTicket = [
    passport.authenticate('getMe', {session: false}),
    createAnswerTicket
]

export const closeTicketsFromAdmin=[
    passport.authenticate('isSuperAdmin', {session: false}),
    setTicketStatusCloseFromAdmin
]

async function createTicket(req, res) {

    const user = req.body.anonymous == 'false' ? await User.findOne({where: {email: req.body.user}}) : null

    const ticket = await Ticket.create({
        subject: req.body.subject, status: 0, UserId: user.id ? `${user.id}` : null, anonymous: false
    })

    await TicketBody.create({
        TicketId: ticket.id, text: req.body.description, side: 0
    })

    res.status(200).send({success: true})

}

async function getUserTickets(req, res) {
    if (!req.user) return res.status(401).send('unauthorized');

    const tickets = await Ticket.findAll({

        where: {UserID: req.user.id }, order: [['createdAt', 'desc'],], include: TicketBody
    })
    res.status(200).send(tickets)
}

async function getAllTickets(req, res) {

    const tickets = await Ticket.findAll({
        where: {status: 0},

        order: [['createdAt', 'desc'],], include: [{model: TicketBody}, {model: User}]
    })
    res.status(200).send(tickets)


}

async function createAnswerTicket(req, res) {


    const seenTicketSide = parseInt(req.body.side) === 1 ? 0 : 1
    const ticketID = parseInt(req.body.ticketId)
    await TicketBody.update(
        {seen: true},
        {
            where: {
                side: seenTicketSide,
                TicketId: ticketID
            },
        },
    );


    const ticketBody = await TicketBody.create({
        TicketId: ticketID,
        text: req.body.text,
        side: req.body.side,
    })
    const tickets = await Ticket.findAll({
        where : {status: 0},
        include: [{model: TicketBody}, {model: User}],
        order: [['createdAt', 'desc'],]
    })
    res.status(200).send(tickets)


}

async function setTicketStatusClose(req, res) {

    await Ticket.update(
        {status: 1},
        {
            where: {
                id: req.body.ticketId,

            },
        },
    );
    const tickets = await Ticket.findAll({

        where: {UserID: req.user.id , status : 0}, order: [['createdAt', 'desc'],], include: TicketBody
    })
    res.status(200).send(tickets)
}
async function setTicketStatusCloseFromAdmin(req, res) {
    await Ticket.update(
        {status: 1},
        {
            where: {
                id: req.body.ticketId,

            },
        },
    );


    const tickets = await Ticket.findAll({
        where: {status: 0},

        order: [['createdAt', 'desc'],], include: [{model: TicketBody}, {model: User}]
    })
    res.status(200).send(tickets)

}


async function getArchiveList(req, res){
    if (!req.user) return res.status(401).send('unauthorized');

    const tickets = await Ticket.findAll({

        where: {UserID: req.user.id , status : 1}, order: [['createdAt', 'desc'],], include: TicketBody
    })
    res.status(200).send(tickets)
}