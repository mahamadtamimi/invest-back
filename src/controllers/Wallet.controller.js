import passport from "passport";
import {Role, Transaction, User, Wallet} from "../models/index.js";
import jwt from "jsonwebtoken";
import {userResource} from "./Users.controller.js";
import transaction from "../models/transaction.js";
import {Op} from "sequelize";
import {underscoredIf} from "sequelize/lib/utils";
import speakeasy from "speakeasy";
import {json, response} from "express";


export const withdrawalList = [
    passport.authenticate('isSuperAdmin', {session: false}),
    getWithdrawalList
]

export const getWalletAddress = [
    passport.authenticate('getMe', {session: false}),
    UserWalletAddress
]


export const withdrawalComplete = [
    passport.authenticate('isSuperAdmin', {session: false}),
    setWithdrawalStatusComplete,
    createPayoutRequest,
    getWithdrawalList
]


export const me = [
    passport.authenticate('getMe', {session: false}),
    getUserWallet

]


export const withdrawal = [
    passport.authenticate('getMe', {session: false}),
    withdrawalWallet
]


export const updateWalletAddress = [
    passport.authenticate('getMe', {session: false}),
    changeWalletAddress
]


export const validateWalletAddress = [
    // passport.authenticate('getMe', {session: false}),
    checkValidateWalletAddress]

export const payout = [
    // passport.authenticate('isSuperAdmin', {session: false}),
    createPayoutRequest
]


async function createPayoutRequest(req, res) {

    if (!parseInt(req.body.id)) return res.status(404).send({massage: 'bad request'})

    const withdrawal = await Transaction.findOne({where: {'id': req.body.id}, include: User})

    const gatewayPrefix = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_URI : process.env.GATEWAY_URI

    const apiKey = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_API_KEY : process.env.GATEWAY_API_KEY

    const authUrl = `${gatewayPrefix}/v1/auth`
    const authOptions = {
        'method': 'POST',

        'headers': {
            'Content-Type': 'application/json'
        }, body: JSON.stringify({
            email: "investition001@gmail.com", password: "L723#$vb"
        })

    };

    const authData = await fetch(authUrl, authOptions);
    const authResult = await authData.json()
    console.log('========================')
    // return res.send(authResult)
    const token = authResult.token
    const url = `${gatewayPrefix}/v1/payout`
    var options = {
        'method': 'POST', 'headers': {
            'Authorization': `Bearer ${token}`, 'x-api-key': apiKey, 'Content-Type': 'application/json'
        }, body: JSON.stringify({
            "ipn_callback_url": "https://nowpayments.io", "withdrawals": [{
                "address": withdrawal.User.walletAddress,
                "currency": "usdttrc20",
                "amount": withdrawal.amount,
                "ipn_callback_url": "https://nowpayments.io"
            },]
        })

    };
    const payoutData = await fetch(url, options);
    const payoutResult = await payoutData.json()
    // console.log('========================')
    console.log(payoutResult)
    return res.send(payoutResult)

}

async function checkValidateWalletAddress(req, res) {

    const gatewayPrefix = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_URI : process.env.GATEWAY_URI

    const apiKey = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_API_KEY : process.env.GATEWAY_API_KEY

    const url = `${gatewayPrefix}/v1/payout/validate-address'`
    var options = {
        'method': 'POST',

        'headers': {
            'x-api-key': apiKey, 'Content-Type': 'application/json'
        }, body: JSON.stringify({
            "address": req.body.address, "currency": "trx", "extra_id": null
        })

    };

    const data = await fetch(url, options);
    const result = await data.json()
    console.log('========================')
    console.log(result)
    res.send(result)

}


async function getUserWallet(req, res) {


    const wallet = await Wallet.findOne({where: {UserId: req.user.id}})

    const user = await User.findOne({where: {'id': req.user.id}})

    const lock = user.googleToken !== null

    res.status(200).send({success: true, wallet: wallet, lock: lock})

}

async function withdrawalWallet(req, res) {


    const wallet = await Wallet.findOne({where: {UserId: req.user.id}})

    const user = await User.findOne({where: {'id': req.user.id}})

    const amount = parseFloat(req.body.amount);

    if (!amount) return res.status(404).send('bad request')
    if (amount > wallet.credit || amount === 0) return res.status(404).send('bad request')

    console.log(user.googleToken)
    console.log(req.body.googleToken)


    if (user.googleToken === null) {
        wallet.set({
            credit: wallet.credit - req.body.amount
        })
        await wallet.save()

        await Transaction.create({
            UserId: req.user.id,
            amount: amount,
            description: `withdrawal`,
            type: 'decrement',
            status: 'waiting for confirmation',
        })

        return res.status(200).send(wallet)
    }else {
        const verified = speakeasy.totp.verify({
            secret: user.googleToken,
            encoding: 'base32',
            token: req.body.googleCode,
            window: 1, // Allow a time window of 1 unit (default is 0)
        });


        if (verified) {

            wallet.set({
                credit: wallet.credit - req.body.amount
            })
            await wallet.save()

            await Transaction.create({
                UserId: req.user.id,
                amount: amount,
                description: `withdrawal`,
                type: 'decrement',
                status: 'waiting for confirmation',
            })

            return res.status(200).send(wallet)
        } else {
            return res.status(402).send('Invalid Auth Code !')
        }
    }





}

async function changeWalletAddress(req, res) {

    User.update({
        walletAddress: req.body.walletAddress,
    }, {where: {id: req.user.id}})


    const {id, username} = req.user
    const payload = {
        user: {
            id, username
        }
    }
    const token = jwt.sign(payload, process.env.JWT_SECRET)

    const user = await User.findOne({
        where: {id: req.user.id}, include: [

            {model: Role}, {model: Wallet}
        ]
    })

    console.log(user)
    const data = userResource(token, user)
    // const data = {}

    res.json(data)
}


async function UserWalletAddress(req, res) {
    res.status(200).send({walletAddress: req.user.walletAddress})


}


async function getWithdrawalList(req, res) {

    const withdrawal = await Transaction.findAll({
        where: {[Op.and]: [{'description': 'withdrawal'}, {'status': 'waiting for confirmation'}]}, include: User
    })
    res.status(200).send(withdrawal)

}


async function setWithdrawalStatusComplete(req, res) {
    if (!parseInt(req.body.id)) return res.status(404).send({massage: 'bad request'})

    const withdrawal = await Transaction.findOne({where: {'id': req.body.id}, include: User})

    withdrawal.set({status: 'complete'})

    await withdrawal.save()


}