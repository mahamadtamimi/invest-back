import {Plan, sequelize, Transaction, User_plan, Wallet} from "../models/index.js";
import moment from "moment";


export async function run(req, res) {


    const userPlan = await User_plan.findAll({include: Plan})

    if (userPlan) {

        userPlan.map(async (item) => {
            if (item.initAmount === 0) return

            const TestToday = new Date();


            const expireDate = new Date(item.expireAt)
            const date1 = new Date(item.startAt);
            const date2 = moment().add('1', 'days').set('hour', 0).set('minute', 1).set('second', 0).toDate();

            console.log(date2)
            // return null

            let Expire_In_Time = expireDate.getTime() - date1.getTime();
            let Expire_In_Days = Math.round(Expire_In_Time / (1000 * 3600 * 24));


            let Difference_In_Time = date2.getTime() - date1.getTime();


            let Difference_In_Days = Math.round(Difference_In_Time / (1000 * 60 * 60 * 24) + 1);


            const dailyProfit = (item.Plan.interestRates / item.Plan.dailyCredit)
            const profit = dailyProfit * item.initAmount / 100;

            const partOfPeriod = // Math.floor
                (Difference_In_Days % item.Plan.withdrawalPeriod)

            console.log('--------------------------------------')
            console.log(Difference_In_Days)
            console.log('--------------------------------------')

            console.log(item.Plan.dailyCredit)
            console.log('--------------------------------------')

            ////  is after expire time

            if (Difference_In_Days < item.Plan.dailyCredit) {

                ////////////////
                console.log('in plan')


                if (partOfPeriod === 0 && Difference_In_Days !== 0) {

                    console.log(`profit and send profit to wallet plan ${moment.utc().format('HH::mm::ss')}`)


                    await Transaction.create({
                        UserId: item.UserId,
                        amount: item.profit + profit,
                        description: `release of investment profit ${item.Plan.name}`,
                        type: 'System internal transaction',
                        status: 'complete',
                    })


                    console.log(`record transaction ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)

                    await Wallet.increment({
                        credit: item.profit + profit
                    }, {
                        where: {
                            UserId: item.UserId
                        }
                    })


                    console.log(`in wallet ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: 0}, {
                        where: {
                            id: item.id
                        }
                    });

                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')

                } else {
                    console.log(`just profit plan ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: item.profit + profit}, {
                        where: {
                            id: item.id
                        }
                    });
                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')
                }


            } else {

                console.log('****------****')
                console.log(`${item.id}`)


                // Then, we do some calls passing this transaction as an option:


                await Transaction.create({
                    UserId: item.UserId,
                    amount: item.initAmount + item.profit + profit,
                    description: `release of investment profit ${item.Plan.name}`,
                    type: 'System internal transaction',
                    status: 'complete',
                })


                await Wallet.increment({
                    credit: item.initAmount + item.profit + profit
                }, {
                    where: {
                        UserId: item.UserId
                    }
                })




                await User_plan.update({profit: 0, initAmount: 0}, {
                    where: {
                        id: item.id
                    }
                });


                // If the execution reaches this line, no errors were thrown.
                // We commit the transaction.


            }


        })
    }
    return res.status(200).send({success: true})
}



export async function run2(req , res) {
    const userPlan = await User_plan.findAll({include: Plan})

    if (userPlan) {

        userPlan.map(async (item) => {
            if (item.initAmount === 0) return

            const TestToday = new Date();


            const expireDate = new Date(item.expireAt)
            const date1 = new Date(item.startAt);
            const date2 = moment().add('2', 'days').set('hour', 0).set('minute', 1).set('second', 0).toDate();

            console.log(date2)
            // return null

            let Expire_In_Time = expireDate.getTime() - date1.getTime();
            let Expire_In_Days = Math.round(Expire_In_Time / (1000 * 3600 * 24));


            let Difference_In_Time = date2.getTime() - date1.getTime();


            let Difference_In_Days = Math.round(Difference_In_Time / (1000 * 60 * 60 * 24) + 1);


            const dailyProfit = (item.Plan.interestRates / item.Plan.dailyCredit)
            const profit = dailyProfit * item.initAmount / 100;

            const partOfPeriod = // Math.floor
                (Difference_In_Days % item.Plan.withdrawalPeriod)

            console.log('--------------------------------------')
            console.log(Difference_In_Days)
            console.log('--------------------------------------')

            console.log(item.Plan.dailyCredit)
            console.log('--------------------------------------')

            ////  is after expire time

            if (Difference_In_Days < item.Plan.dailyCredit) {

                ////////////////
                console.log('in plan')


                if (partOfPeriod === 0 && Difference_In_Days !== 0) {

                    console.log(`profit and send profit to wallet plan ${moment.utc().format('HH::mm::ss')}`)


                    await Transaction.create({
                        UserId: item.UserId,
                        amount: item.profit + profit,
                        description: `release of investment profit ${item.Plan.name}`,
                        type: 'System internal transaction',
                        status: 'complete',
                    })


                    console.log(`record transaction ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)

                    await Wallet.increment({
                        credit: item.profit + profit
                    }, {
                        where: {
                            UserId: item.UserId
                        }
                    })


                    console.log(`in wallet ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: 0}, {
                        where: {
                            id: item.id
                        }
                    });

                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')

                } else {
                    console.log(`just profit plan ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: item.profit + profit}, {
                        where: {
                            id: item.id
                        }
                    });
                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')
                }


            } else {

                console.log('****------****')
                console.log(`${item.id}`)


                // Then, we do some calls passing this transaction as an option:


                await Transaction.create({
                    UserId: item.UserId,
                    amount: item.initAmount + item.profit + profit,
                    description: `release of investment profit ${item.Plan.name}`,
                    type: 'System internal transaction',
                    status: 'complete',
                })


                await Wallet.increment({
                    credit: item.initAmount + item.profit + profit
                }, {
                    where: {
                        UserId: item.UserId
                    }
                })




                await User_plan.update({profit: 0, initAmount: 0}, {
                    where: {
                        id: item.id
                    }
                });


                // If the execution reaches this line, no errors were thrown.
                // We commit the transaction.


            }


        })
    }
    return res.status(200).send({success: true})
}


export async function run3(req , res) {
    const userPlan = await User_plan.findAll({include: Plan})


    if (userPlan) {

        userPlan.map(async (item) => {
            if (item.initAmount === 0) return

            const TestToday = new Date();


            const expireDate = new Date(item.expireAt)
            const date1 = new Date(item.startAt);
            const date2 = moment().add('3', 'days').set('hour', 0).set('minute', 1).set('second', 0).toDate();

            console.log(date2)
            // return null

            let Expire_In_Time = expireDate.getTime() - date1.getTime();
            let Expire_In_Days = Math.round(Expire_In_Time / (1000 * 3600 * 24));


            let Difference_In_Time = date2.getTime() - date1.getTime();


            let Difference_In_Days = Math.round(Difference_In_Time / (1000 * 60 * 60 * 24) + 1);


            const dailyProfit = (item.Plan.interestRates / item.Plan.dailyCredit)
            const profit = dailyProfit * item.initAmount / 100;

            const partOfPeriod = // Math.floor
                (Difference_In_Days % item.Plan.withdrawalPeriod)

            console.log('--------------------------------------')
            console.log(Difference_In_Days)
            console.log('--------------------------------------')

            console.log(item.Plan.dailyCredit)
            console.log('--------------------------------------')

            ////  is after expire time

            if (Difference_In_Days < item.Plan.dailyCredit) {

                ////////////////
                console.log('in plan')


                if (partOfPeriod === 0 && Difference_In_Days !== 0) {

                    console.log(`profit and send profit to wallet plan ${moment.utc().format('HH::mm::ss')}`)


                    await Transaction.create({
                        UserId: item.UserId,
                        amount: item.profit + profit,
                        description: `release of investment profit ${item.Plan.name}`,
                        type: 'System internal transaction',
                        status: 'complete',
                    })


                    console.log(`record transaction ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)

                    await Wallet.increment({
                        credit: item.profit + profit
                    }, {
                        where: {
                            UserId: item.UserId
                        }
                    })


                    console.log(`in wallet ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: 0}, {
                        where: {
                            id: item.id
                        }
                    });

                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')

                } else {
                    console.log(`just profit plan ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: item.profit + profit}, {
                        where: {
                            id: item.id
                        }
                    });
                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')
                }


            } else {

                console.log('****------****')
                console.log(`${item.id}`)


                // Then, we do some calls passing this transaction as an option:


                await Transaction.create({
                    UserId: item.UserId,
                    amount: item.initAmount + item.profit + profit,
                    description: `release of investment profit ${item.Plan.name}`,
                    type: 'System internal transaction',
                    status: 'complete',
                })


                await Wallet.increment({
                    credit: item.initAmount + item.profit + profit
                }, {
                    where: {
                        UserId: item.UserId
                    }
                })




                await User_plan.update({profit: 0, initAmount: 0}, {
                    where: {
                        id: item.id
                    }
                });


                // If the execution reaches this line, no errors were thrown.
                // We commit the transaction.


            }


        })
    }
    return res.status(200).send({success: true})
}


export async function run4(req , res) {
    const userPlan = await User_plan.findAll({include: Plan})

    if (userPlan) {

        userPlan.map(async (item) => {
            if (item.initAmount === 0) return

            const TestToday = new Date();


            const expireDate = new Date(item.expireAt)
            const date1 = new Date(item.startAt);
            const date2 = moment().add('4', 'days').set('hour', 0).set('minute', 1).set('second', 0).toDate();

            console.log(date2)
            // return null

            let Expire_In_Time = expireDate.getTime() - date1.getTime();
            let Expire_In_Days = Math.round(Expire_In_Time / (1000 * 3600 * 24));


            let Difference_In_Time = date2.getTime() - date1.getTime();


            let Difference_In_Days = Math.round(Difference_In_Time / (1000 * 60 * 60 * 24) + 1);


            const dailyProfit = (item.Plan.interestRates / item.Plan.dailyCredit)
            const profit = dailyProfit * item.initAmount / 100;

            const partOfPeriod = // Math.floor
                (Difference_In_Days % item.Plan.withdrawalPeriod)

            console.log('--------------------------------------')
            console.log(Difference_In_Days)
            console.log('--------------------------------------')

            console.log(item.Plan.dailyCredit)
            console.log('--------------------------------------')

            ////  is after expire time

            if (Difference_In_Days < item.Plan.dailyCredit) {

                ////////////////
                console.log('in plan')


                if (partOfPeriod === 0 && Difference_In_Days !== 0) {

                    console.log(`profit and send profit to wallet plan ${moment.utc().format('HH::mm::ss')}`)


                    await Transaction.create({
                        UserId: item.UserId,
                        amount: item.profit + profit,
                        description: `release of investment profit ${item.Plan.name}`,
                        type: 'System internal transaction',
                        status: 'complete',
                    })


                    console.log(`record transaction ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)

                    await Wallet.increment({
                        credit: item.profit + profit
                    }, {
                        where: {
                            UserId: item.UserId
                        }
                    })


                    console.log(`in wallet ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: 0}, {
                        where: {
                            id: item.id
                        }
                    });

                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')

                } else {
                    console.log(`just profit plan ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: item.profit + profit}, {
                        where: {
                            id: item.id
                        }
                    });
                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')
                }


            } else {

                console.log('****------****')
                console.log(`${item.id}`)


                // Then, we do some calls passing this transaction as an option:


                await Transaction.create({
                    UserId: item.UserId,
                    amount: item.initAmount + item.profit + profit,
                    description: `release of investment profit ${item.Plan.name}`,
                    type: 'System internal transaction',
                    status: 'complete',
                })


                await Wallet.increment({
                    credit: item.initAmount + item.profit + profit
                }, {
                    where: {
                        UserId: item.UserId
                    }
                })




                await User_plan.update({profit: 0, initAmount: 0}, {
                    where: {
                        id: item.id
                    }
                });


                // If the execution reaches this line, no errors were thrown.
                // We commit the transaction.


            }


        })
    }
    return res.status(200).send({success: true})
}