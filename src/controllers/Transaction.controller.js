import {Transaction, User} from "../models/index.js";

export const getAllTransaction = [getTransaction]

async function getTransaction(req, res) {
    const transaction = await Transaction.findAll({
        include: [{
            model: User
        }] ,
        order: [['updatedAt', 'DESC']]
    })



    res.json(transaction)
}