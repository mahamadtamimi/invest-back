import passport from "passport";
import nodemailer from "nodemailer";
import {Otp, Role, User, Wallet} from "../models/index.js";
import moment from "moment";
import jwt from "jsonwebtoken";
import {userResource} from "./Users.controller.js";
import qrcode from "qrcode";
import speakeasy from "speakeasy";

import {generate_secret_key} from "../server/generate_secret_key.js";
import {id_ID} from "@faker-js/faker";
import {main} from "./email.controller.js";


export const getCode = [
    passport.authenticate('getMe', {session: false}),
    createCodeInDatabase
]

export const verifyCode = [
    passport.authenticate('getMe', {session: false}),
    checkVerifyCode]


export const forgotPassword = [sendChangePasswordCode]

export const forgotPasswordGetCode = [sendNewCode]

export const changePassword = [
    changePasswordAndValidateCode
]


async function changePasswordAndValidateCode(req, res) {

    console.log(req.body)
    const user = await User.findOne({
        where: {email: req.body.mail,},
        include: [{model: Wallet}, {model: Role}]
    })

    if (!user) return res.status(404).send('bad request')


    const code = await Otp.findAll({where: {UserId: user.id}, order: [['createdAt', 'DESC']]})

    if (parseInt(code[0].code) === parseInt(req.body.code)) {
        user.password = req.body.password
        user.save()
        const id = user.id
        const username = user.username
        const payload = {
            user: {
                id, username
            }
        }
        const token = jwt.sign(payload, process.env.JWT_SECRET)

        const data = userResource(token, user)


        res.json({success: true, user: data})
    } else {
        res.status(200).send({
            success: false,
            error: 'invalid code'
        })
    }


}

async function sendNewCode(req, res) {
    const user = await User.findOne({where: {email: req.body.mail}})

    if (!user) {
        return res.status(200).send({success: false, error: "User not found!"})
    }


    const minm = 10000;
    const maxm = 99999;
    const codeGenerated = Math.floor(Math
        .random() * (maxm - minm + 1)) + minm;

    const codeModel = await Otp.create({
        UserId: user.id, code: codeGenerated
    })
    console.log('=============================')
    console.log(user.email)
    console.log('=============================')

    console.log(codeGenerated)
    console.log('=============================')

    main(codeGenerated , user.email).catch(console.error);
    return res.status(200).json({success: true})

}


async function sendChangePasswordCode(req, res) {
    const user = await User.findOne({where: {email: req.body.email}})

    if (!user) {
        return res.status(200).send({success: false, error: "User not found!"})
    }


    const minm = 10000;
    const maxm = 99999;
    const codeGenerated = Math.floor(Math
        .random() * (maxm - minm + 1)) + minm;

    const codeModel = await Otp.create({
        UserId: user.id, code: codeGenerated
    })

    main(codeGenerated , user.email).catch(console.error);

    return res.status(200).json({success: true})


}


async function createCodeInDatabase(req, res) {

    const minm = 10000;
    const maxm = 99999;
    const codeGenerated = Math.floor(Math
        .random() * (maxm - minm + 1)) + minm;

    const codeModel = await Otp.create({
        UserId: req.user.id, code: codeGenerated
    })

    main(codeGenerated , req.user.email).catch(console.error);
    console.log('=============================')
    console.log(req.user.email)
    console.log('=============================')

    console.log(codeGenerated)
    console.log('=============================')

    return res.status(200).json({success: true})


}


async function checkVerifyCode(req, res) {


    const code = await Otp.findAll({where: {UserId: req.user.id}, order: [['createdAt', 'DESC']]})

    if (parseInt(code[0].code) === parseInt(req.body.code)) {
        const courentUser = await User.findOne({where: {id: req.user.id}})

        await User.update(
            {verifyAt: new Date()},
            {
                where: {
                    id: req.user.id,
                },
            },
        );

        const {id, username} = req.user
        const payload = {
            user: {
                id, username
            }
        }
        const token = jwt.sign(payload, process.env.JWT_SECRET)

        const data = userResource(token, req.user)

        // verifyAt
        return res.status(200).send({success: true, user: data})
        // courentUser.verifyAt = moment().toISOString()
        // courentUser.save()
    } else {
        return res.status(404).send({success: false})
    }


}


export const changePasswordFromDashboard = [
    passport.authenticate('getMe', {session: false}),
    changePasswordInDb
]

async function changePasswordInDb(req, res) {
    try {
        const user = await User.findOne({where: {id: req.user.id}})
        user.password = req.body.password
        user.save()
        return res.status(200).send({success: true})

    } catch (err) {
        return res.status(200).send({success: false})
    }

}

export const twoVerification = [
    passport.authenticate('getMe', {session: false}),
    createTwoVerifiaction
]

async function createTwoVerifiaction(req, res) {
    const secretKey = generate_secret_key();
    console.log(secretKey);

    qrcode.toDataURL(secretKey.otpauth_url, (err, imageUrl) => {
        if (err) {
            console.error('Error generating QR code:', err);
            return res.status(400).send({success: false});

        }
        console.log(imageUrl)
        return res.status(200).send({success: true, data: imageUrl, base32: secretKey.base32});
        // you can then display the QR by adding'<img src="' + imageUrl + '">';
    });
}


export const twoVerificationVerify = [
    passport.authenticate('getMe', {session: false}),
    twoVerificationVerifyCheckCode
]

async function twoVerificationVerifyCheckCode(req, res) {


    const user = await User.findOne({where: {'id': req.user.id}});
    if (!user) return res.status(404).send({success: false, error: 'User does not exist!'});

    const hasValidPassword = user.isValidPassword(req.body.password)
    if (!hasValidPassword) return res.status(400).send({success: false, error: 'Passwords do not match!'});


    const userToken = req.body.code; // OTP entered by the user
    console.log(userToken)

    const verified = speakeasy.totp.verify({
        secret: req.body.secret,
        encoding: 'base32',
        token: userToken,
        window: 1, // Allow a time window of 1 unit (default is 0)
    });

    if (verified) {

        await User.update({
            googleToken: req.body.secret,
        } , {where : {'id': req.user.id}})

        res.status(200).send({success: true})
    } else {
        res.status(401).send({data: 'OTP verification failed. Please try again.'});
    }
}