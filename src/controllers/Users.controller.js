import {Otp, Plan, Role, Transaction, User, User_plan, User_Role, Wallet} from "../models/index.js";
import passport from "passport";
import jwt from 'jsonwebtoken'
import 'dotenv'
import {Op, where} from "sequelize";
import {createPlan} from "./Plan.controller.js";
import moment from "moment";
import nodemailer from "nodemailer";
import {updateWalletAddress} from "./Wallet.controller.js";
import speakeasy from "speakeasy";
import {main} from "./email.controller.js";


export const getMe = [passport.authenticate('getMe', {session: false}), returnAuthInfo

]


export const userTransactions = [
    passport.authenticate('getMe', {session: false}),
    getUserTransactions
]

export const userPlan = [passport.authenticate('getMe', {session: false}), getUserPlan]

export const userWallets = [passport.authenticate('isSuperAdmin', {session: false}), sumUserWallet]

async function getUserPlan(req, res) {
    const user = req.user
    const userPlan = await User_plan.findAll({
        where: {
            UserId: user.id, initAmount: {[Op.gt]: 0},

        }, include: Plan
    })
    res.status(200).json(userPlan)


}


export const list = [
    passport.authenticate('isSuperAdmin', {session: false}),
    getAllUser
]

export const signup = [
    passport.authenticate('signup', {session: false}),
    returnCreatedUser
]


export const login = [
    passport.authenticate('login', {session: false,}),
    returnAuthInfo
]


export const userActivePlan = [passport.authenticate('isSuperAdmin', {session: false}), getActivePlan]


export async function getActivePlan(req, res) {
    const activePlan = await User_plan.findAll({
        where: {
            expireAt: {

                [Op.gte]: moment().toISOString()

            }
        }, include: [{model: User}, {model: Plan}]
    })


    res.send(activePlan)

}


export async function getUser({username}) {

    // console.log(user)
    return await User.findOne({
        where: {username}, include: [{
            model: Role,
        }, {
            model: Wallet,

        },]
    });
}


export async function createUser({username, firstName, lastName, email, password}) {


    const user = await User.create({
        username, firstName, lastName, email, password
    })

    // user.password = password;
    // user.save()


    const role = await User_Role.create({UserId: user.id, RoleId: 2})
    const wallet = await Wallet.create({UserId: user.id, credit: 0})


    const minm = 10000;
    const maxm = 99999;
    const codeGenerated = Math.floor(Math
        .random() * (maxm - minm + 1)) + minm;

    const codeModel = await Otp.create({
        UserId: user.id, code: codeGenerated
    })

    main(codeGenerated, email).catch(console.error);

    return await User.findOne({where: {username}, include: [{model: Role}, {model: Wallet}]})

}


async function returnCreatedUser(req, res) {

    if (req.user.id === undefined) {

        res.status(409).send('User early existed !')
    } else {
        const {id, username} = req.user
        const payload = {
            user: {
                id, username
            }
        }
        const token = jwt.sign(payload, process.env.JWT_SECRET)

        const data = userResource(token, req.user)


        res.json(data)

    }


}


function returnAuthInfo(req, res) {

    if (!req.user) {
        return res.status(401).send('Invalid Username Or Password')
    }
    const {id, username} = req.user
    const payload = {
        user: {
            id, username
        }
    }
    const token = jwt.sign(payload, process.env.JWT_SECRET)

    const data = userResource(token, req.user)


    res.json(data)
}


export function fieldResponse(req, res) {
    res.status(402).send('Invalid username or password !')
}

export function userResource(token, user) {
    return {
        'firstName': user.firstName,
        'lastName': user.lastName,
        'email': user.email,
        'active': user.walletAddress,
        'wallet': user.Wallet.credit,
        'redirect': user.Roles[0].role ? user.Roles[0].role : 'User',
        'verifyAt': user.verifyAt,
        'lock': user.googleToken !== null,
        'token': token
    }
}


async function getAllUser(req, res) {

    const users = await User.findAll({
        include: Role
    });

    res.status(200).json(users)
}


async function getUserTransactions(req, res) {
    const transaction = await Transaction.findAll({
        where: {UserId: req.user.id}, order: [['createdAt', 'DESC']]
    })

    res.status(200).send(transaction)

}

async function sumUserWallet(req, res) {

    const userWallets = await Wallet.findAll()
    var sum = 0
    userWallets.forEach((item) => {
        sum = sum + item.credit
    })
    res.send({total: sum})
}


export const createNewUser = [
    passport.authenticate('isSuperAdmin', {session: false}),
    createUserFromAdmin
]

async function createUserFromAdmin(req, res) {
    const user = await User.findOne({where: {email: req.body.email}})

    if (user) return res.status(200).send({success: false, error: 'User already exists!'})


    const newUser = await User.create({
        username: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        verifyAt: new Date(),
    })


    if (req.body.role === 'admin') {
        const role = await User_Role.create({UserId: newUser.id, RoleId: 1})
    } else {
        const role = await User_Role.create({UserId: newUser.id, RoleId: 2})

    }


    const wallet = await Wallet.create({UserId: newUser.id, credit: 0})

    return res.status(200).send({success: true})
}


export const getUserInfo = [
    passport.authenticate('isSuperAdmin', {session: false}),
    collectUserInfo
]

async function collectUserInfo(req, res) {

    const user = await User.findOne({
        where: {id: req.body.id},
        include: [{model: Wallet}, {model: Role}]
    })

    const userPlan = await User_plan.findAll({
        where: {UserId: user.id, initAmount: {[Op.gt]: 0}},
        include: [{model: Plan}, {model: User}]
    })
    return res.status(200).send({success: true, user: user, plans: userPlan})


}


export const updateUserInfo = [
    passport.authenticate('isSuperAdmin', {session: false}),
    setNewWalletAmountForUser
]


async function setNewWalletAmountForUser(req, res) {
    const wallet = await Wallet.update({credit: req.body.wallet}, {
        where: {UserId: req.body.id},
    })
    return res.status(200).send({success: true, data: wallet})
}

export const updateUserWalletAddress = [
    passport.authenticate('isSuperAdmin', {session: false}),
    updateWalletAddressFromAdmin


]

async function updateWalletAddressFromAdmin(req, res) {


    const user = await User.update({walletAddress: req.body.walletAddress}, {where: {id: req.body.id}})
    return res.status(200).send({success: true, data: user})
}


export const checkGoogleVerificate = [
    passport.authenticate('getMe', {session: false}),
    unLockGoogleCode
]


async function unLockGoogleCode(req, res) {

    const verified = speakeasy.totp.verify({
        secret: req.user.googleToken,
        encoding: 'base32',
        token: req.body.code,
        window: 1, // Allow a time window of 1 unit (default is 0)
    });

    res.status(200).send({success: verified})

}


export const disbaleGoogleVerificate = [
    passport.authenticate('getMe', {session: false}),
    deleteGoogleSecret
]


async function deleteGoogleSecret(req, res) {
    const user = await User.findOne({where: {id: req.user.id}})
    const hasValidPassword = user.isValidPassword(req.body.password)
    if (!hasValidPassword) return res.status(400).send({success: false, error: 'Code Or Password is not valid!'});

    const verified = speakeasy.totp.verify({
        secret: req.user.googleToken,
        encoding: 'base32',
        token: req.body.code,
        window: 1, // Allow a time window of 1 unit (default is 0)
    });

    if (verified) {
        await User.update({
            googleToken: null,
        }, {where: {id: req.user.id}})
    }else {
        if (!hasValidPassword) return res.status(400).send({success: false, error: 'Code Or Password is not valid!'});

    }

    return res.status(200).send({success: true})


}