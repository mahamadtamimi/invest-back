import passport from "passport";
import {validator} from "sequelize/lib/utils/validator-extras";
import {validationResult} from "express-validator";
import {Plan} from "../models/index.js";
import res from "express/lib/response.js";
import {where} from "sequelize";

export async function planList(req, res) {
    const planList = await Plan.findAll({where: {active : true}});

    return res.send(planList)
}

export const updatePlan = [
    passport.authenticate('isSuperAdmin', {session: false}),
    updatePlanCH
]


export const planStore = [
    passport.authenticate('isSuperAdmin', {session: false}),
    createPlan
]


export const showPlan = [
    passport.authenticate('isSuperAdmin', {session: false}),
    getPlan
]


export async function createPlan(req, res) {
    if (!req.user) {
        return res.status(401).send('Unauthorized !')
    }

    const plan = await Plan.create({
        name: req.body.name,
        minimumDeposit: req.body.minimumDeposit,
        dailyCredit: req.body.dailyCredit,
        withdrawalPeriod: req.body.withdrawalPeriod,
        interestRates: req.body.interestRates,
        description : req.body.description ,
        active: 1
    })

    return res.status(200).json({success: true, plan: plan})

}

async function getPlan(req, res) {

    const plan = await Plan.findOne({where: {id: req.body.id}})


    res.status(200).send(plan)

}


async function updatePlanCH(req, res) {
    const plan = await Plan.update({
        name: req.body.name,
        minimumDeposit: req.body.minimumDeposit,
        dailyCredit: req.body.dailyCredit,
        withdrawalPeriod: req.body.withdrawalPeriod,
        interestRates: req.body.interestRates,
    }, {
        where: {
            id: req.body.id
        }
    });

    res.status(200).send({success: true})

}


export const destroyPlan = [
    passport.authenticate('isSuperAdmin', {session: false}),
    deletePlan
]


async function deletePlan(req, res) {
    await Plan.destroy({
        where: {
            id: req.body.id
        }
    });
    res.status(200).send({success: true})
}


export const changeActivate = [
    passport.authenticate('isSuperAdmin', {session: false}),
    toggleActivate
]

async function toggleActivate(req, res) {
    const plan = await Plan.findOne({where: {id: req.body.id}})
    plan.set({active: !plan.active})
    await plan.save()

    res.status(200).send({success: true})


}


export const planListAll = [
    passport.authenticate('isSuperAdmin', {session: false}),
    getAllPlan
]


async function getAllPlan(req , res){
    const plans = await Plan.findAll({})
    res.status(200).send(plans)
}