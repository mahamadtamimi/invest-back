import {Role, User, User_Role, Wallet} from "../models/index.js";
import user from "../models/user.js";

export const seeder = [
    createRole,

]

async function createRole(req, res) {
    const adminRole = await Role.findOne({where: {role: 'admin'}})
    const userRole = await Role.findOne({where: {role: 'user'}})

    if (!adminRole) {
        await Role.create({id: 1, role: 'Admin'})
        console.log('Role :Admin created')
    } else {
        console.log('Role :Admin early existed')
    }
    if (!userRole) {
        await Role.create({id: 2, role: 'User'})
        console.log('Role :User created')
    } else {
        console.log('Role :User early existed')
    }


    const adminUser = await User.findOne({where: {username: 'admin@example.com'}})
    if (!adminUser) {

        const user = await User.create({
            username: 'admin@example.com',
            email: 'admin@example.com',
            firstName: 'super',
            lastName: 'admin',
            password: 'password'


        })
        const role = await User_Role.create({UserId: user.id, RoleId: 1})
        const wallet = await Wallet.create({UserId: user.id, credit: 0})

        console.log('super admin created !')

    } else {
        console.log('super admin early existed !')
    }
}

