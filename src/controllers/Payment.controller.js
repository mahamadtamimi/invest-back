import passport from "passport";
import {createPlan} from "./Plan.controller.js";
import {Debit, Payment, Plan, ShowTransActions, Transaction, User_plan, Wallet} from "../models/index.js";
// import moment from "moment";
import 'dotenv/config'

import moment from 'moment-timezone';
import debit from "../models/debit.js";
import wallet from "../models/wallet.js";
import {AdminEmail, main} from "./email.controller.js";
import payment from "../models/payment.js";


export const getPaymentLink = [passport.authenticate('getMe', {session: false}), createPaymentLink]


export const verify = [passport.authenticate('getMe', {session: false}), verifyTransaction]


export const balance = [passport.authenticate('isSuperAdmin', {session: false}), getBalance]


async function getBalance(req, res) {
    try {
        const gatewayPrefix = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_URI : process.env.GATEWAY_URI

        const apiKey = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_API_KEY : process.env.GATEWAY_API_KEY


        const url = `${gatewayPrefix}/v1/balance`
        var options = {
            'method': 'GET', 'headers': {
                'x-api-key': apiKey
            }
        };


        const payoutUrl = `${gatewayPrefix}/v1/payout?status=CREATING`

        const data = await fetch(url, options);
        const payoutData = await fetch(payoutUrl, options)

        const result = await data.json()
        const payoutResult = await payoutData.json()
        console.log('========================')
        console.log(result)
        console.log('========================')
        console.log(payoutResult)

        res.send({balance: result, payoutPending: payoutResult})
    } catch (e) {
        res.send({error: true, massage: e})
    }


}


async function createPaymentLink(req, res) {
    try {
        const gatewayPrefix = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_URI : process.env.GATEWAY_URI

        const apiKey = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_API_KEY : process.env.GATEWAY_API_KEY


        if (req.body.gateway == 'wallet') {
            const wallet = await Wallet.findOne({where: {UserID: req.user.id}})
            if (wallet.credit >= req.body.amount) {
                // #toDo   1.kasre az wallet
                wallet.credit = wallet.credit - req.body.amount
                wallet.save()


                // #toDo   2.active plan
                const plan = await Plan.findOne({where: {id: req.body.planId}})
                const date = moment.utc().format('YYYY-MM-DD HH:mm:ss')


                const expireDay = moment().utc().add((plan.dailyCredit), 'day').format('YYYY-MM-DD HH:mm:ss')


                await User_plan.create({
                    UserId: req.user.id,
                    PlanId: req.body.planId,
                    initAmount: req.body.amount,
                    profit: 0,
                    startAt: date,
                    expireAt: expireDay,
                })

                // #toDo   3.set trasaction


                await Transaction.create({
                    UserId: req.user.id,
                    amount: req.body.amount,
                    description: `buy plan ${plan.name}`,
                    type: `increment from ${req.body.gateway}`,
                    status: 'complete',
                })


                // await ShowTransActions.create({
                //
                // })


                // #toDo   4.set tank you success
                return res.send({success: true, url: `${process.env.UI_PATH}/auth/dashboard/tank-you-success`})
            }
            if (wallet.credit < req.body.amount) {

                const amount = req.body.amount - wallet.credit

                var optionsInvoice = {
                    'method': 'POST', 'headers': {
                        'x-api-key': apiKey, 'Content-Type': 'application/json'
                    }, body: JSON.stringify({
                        "price_amount": amount,
                        "price_currency": "usd",
                        "order_id": "RGDBP-21314",
                        "order_description": "investitionhub",
                        "ipn_callback_url": `${process.env.UI_PATH}/auth/dashboard/tank-you`,
                        "success_url": `${process.env.UI_PATH}/auth/dashboard/tank-you`,
                        "cancel_url": `${process.env.UI_PATH}/auth/dashboard/tank-you`,
                    })

                };

                const data = await fetch(`${gatewayPrefix}/v1/invoice`, optionsInvoice);
                const result = await data.json()


                const paymentModel = await Payment.create({
                    userId: req.user.id,
                    planId: req.body.planId,
                    amount: amount,
                    authority: result.id,
                    gateway: req.body.gateway,
                    status: 0
                })


                await Debit.create({
                    PaymentId: paymentModel.id, debit: wallet.credit
                })


                wallet.credit = 0
                wallet.save()


                return res.send({success: true, url: result['invoice_url']})


            }
        } else {


        }

        ///////////////////////////////////////////////////////////
        var optionsInvoice = {
            'method': 'POST', 'headers': {
                'x-api-key': apiKey, 'Content-Type': 'application/json'
            }, body: JSON.stringify({
                "price_amount": req.body.amount,
                "price_currency": "usd",
                "order_id": "RGDBP-21314",
                "order_description": "investitionhub",
                "ipn_callback_url": `${process.env.UI_PATH}/auth/dashboard/tank-you`,
                "success_url": `${process.env.UI_PATH}/auth/dashboard/tank-you`,
                "cancel_url": `${process.env.UI_PATH}/auth/dashboard/tank-you`,
            })

        };

        const data = await fetch(`${gatewayPrefix}/v1/invoice`, optionsInvoice);
        const result = await data.json()
        console.log(result)

        await Payment.create({
            userId: req.user.id,
            planId: req.body.planId,
            amount: req.body.amount,
            authority: result.id,
            gateway: req.body.gateway,
            status: 0
        })
        var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress
        console.log(ip)

        res.send({success: true, url: result['invoice_url']})

    } catch (e) {
        console.log(e)
        res.send({success: false, error: ''})
    }

}


async function verifyTransaction(req, res) {
    try {
        const gatewayPrefix = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_URI : process.env.GATEWAY_URI

        const apiKey = process.env.GATEWAY_MOOD === 'sandbox' ? process.env.SANDBOX_GATEWAY_API_KEY : process.env.GATEWAY_API_KEY

        const url = `${gatewayPrefix}/v1/payment/${req.body.authority}`
        var options = {

            'headers': {
                'x-api-key': apiKey
            }
        };

        const data = await fetch(url, options);

        const result = await data.json()


        if (result.payment_status === 'finished') {

            const payment = await Payment.findOne({where: {authority: result.invoice_id}, include: Debit})
            const plan = await Plan.findOne({where: {id: payment.planId}})

            console.log(result.price_amount.toFixed(3))
            console.log('======================')

            console.log(payment.amount)


            if (payment.userId === req.user.id && parseFloat(payment.amount) === parseFloat(result.price_amount.toFixed(3)) && !payment.status) {
                payment.set({
                    status: 1, refId: result.payment_id
                });

                await payment.save();


                const amount = payment.gateway === 'wallet' ? result.pay_amount + payment.Debit.debit : result.pay_amount

                const date = moment.utc().format('YYYY-MM-DD HH:mm:ss')


                const expireDay = moment().utc().add((plan.dailyCredit), 'day').format('YYYY-MM-DD HH:mm:ss')


                await User_plan.create({
                    UserId: req.user.id,
                    PlanId: payment.planId,
                    initAmount: amount.toFixed(2),
                    profit: 0,
                    startAt: date,
                    expireAt: expireDay,
                })


                await Transaction.create({
                    UserId: req.user.id,
                    amount: amount,
                    description: `buy plan ${plan.name}`,
                    type: 'increment from gateway',
                    status: 'complete',
                })


                ////////////// from wallet

                if (payment.gateway === 'wallet') {
                    await Transaction.create({
                        UserId: req.user.id,
                        amount: payment.Debit.debit,
                        description: `buy plan ${plan.name}`,
                        type: 'increment from wallet',
                        status: 'complete',
                    })
                }

                await AdminEmail(`Deposit to the account  ${amount.toFixed(2)}`)
                return res.send({success: true})

            } else {
                return res.send({success: false, error: 404})
            }

        } else if (result.payment_status === 'confirmed') {
            return res.send({status: 'confirmed'})
        } else {
            return res.send({success: false})
        }

    } catch (e) {
        console.log(e)
        return res.send({success: false, error: 1})
    }
}