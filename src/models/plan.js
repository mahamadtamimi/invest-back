import {DataTypes} from "sequelize";
import bcrypt from "bcrypt";

export default function (sequelize) {


    return sequelize.define('Plan', {
        name: {
            type: DataTypes.STRING
        }, minimumDeposit: {
            type: DataTypes.INTEGER
        }, dailyCredit: {
            type: DataTypes.INTEGER
        }, withdrawalPeriod: {
            type: DataTypes.INTEGER
        }, description : {
            type: DataTypes.TEXT
        } ,active: {
            type: DataTypes.BOOLEAN, default: 1
        }, interestRates: {
            type: DataTypes.FLOAT
        },
    })


};

