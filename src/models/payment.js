import {DataTypes} from "sequelize";

export default function (sequelize) {


    return sequelize.define('Payment', {
            userId: {
                type: DataTypes.INTEGER
            },
            planId: {
                type: DataTypes.INTEGER
            },
            amount: {
                type: DataTypes.FLOAT
            },
            authority: {
                type: DataTypes.STRING
            },
            gateway: {
                type: DataTypes.STRING,
            },
            status: {
                type: DataTypes.BOOLEAN,
                default: 0
            },
            refId: {
                type: DataTypes.STRING,
                allowNull: true
            },
        }
    )


};

