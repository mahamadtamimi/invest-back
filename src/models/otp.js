import {DataTypes} from "sequelize";

export default function (sequelize) {


  return sequelize.define('Otps', {
        UserId: {
          allowNull: false,
          type: DataTypes.INTEGER
        },
        code: {
          type: DataTypes.INTEGER
        },
      }
  )
};

