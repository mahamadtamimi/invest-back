import {DataTypes} from "sequelize";


export default function (sequelize) {


    return sequelize.define('User_Plan', {
            UserId: {
                type: DataTypes.INTEGER
            },
            PlanId: {
                type: DataTypes.INTEGER
            },
            initAmount: {
                type: DataTypes.INTEGER
            },
            profit: {
                type: DataTypes.INTEGER
            },
            startAt: {
                allowNull: false,
                type: DataTypes.STRING
            },
            expireAt: {
                allowNull: false,
                type: DataTypes.STRING
            },
        }
    )


};

