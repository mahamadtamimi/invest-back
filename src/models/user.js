import {DataTypes} from "sequelize";
import bcrypt from "bcrypt";
import Role from "./role.js";


async function hashPassword(user) {
    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(user.password, salt)
}

export default function (sequelize) {


    const User = sequelize.define('User', {
        'username': {
            type: DataTypes.STRING
        }, 'firstName': {
            type: DataTypes.STRING
        }, 'lastName': {
            type: DataTypes.STRING
        }, 'email': {
            type: DataTypes.STRING
        }, 'walletAddress': {
            type: DataTypes.STRING
        }, 'password': {
            type: DataTypes.STRING
        }, 'verifyAt': {
            type: DataTypes.DATE, allowNull: true
        }, 'googleToken': {
            type: DataTypes.STRING, allowNull: true
        }
    }, {
        hooks: {
            beforeCreate: hashPassword, beforeUpdate: hashPassword,

        },
    })

    User.prototype.isValidPassword = function (password) {
        return bcrypt.compareSync(password, this.password)
    }

    User.prototype.isAdmin = function () {

        const userRole = this.Roles.map((item) => {
            return item.role
        })


        return (userRole.indexOf('Admin') >= 0)

    }

    User.associate = function (models) {
        User.belongsToMany(Role, {through: "userRoles"});
    };
    return User
};

