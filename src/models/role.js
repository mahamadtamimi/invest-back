import {DataTypes} from "sequelize";
import User from "./user.js";


export default function (sequelize) {


    const Role = sequelize.define('Role', {
            role: {
                type: DataTypes.STRING
            },
        }
    )
    Role.associate = function(models) {
        Role.belongsToMany(User, { through: "userRoles" });
    };
    return Role
};

