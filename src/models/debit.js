import {DataTypes} from "sequelize";

export default function (sequelize) {


    return sequelize.define('Debits', {
            PaymentId: {
                allowNull: false,
                type: DataTypes.BOOLEAN
            },
            debit: {
                type: DataTypes.FLOAT
            },
        }
    )

};

