import {DataTypes} from "sequelize";
import bcrypt from "bcrypt";

export default function (sequelize) {


    return sequelize.define('Tickets', {
        UserId: {
            type: DataTypes.INTEGER
        }, subject: {
            type: DataTypes.TEXT
        }, status: {
            type: DataTypes.BOOLEAN
        }, anonymous: {
            type: DataTypes.BOOLEAN
        },
    })


};

