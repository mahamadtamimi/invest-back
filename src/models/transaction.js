import {DataTypes} from "sequelize";

export default function (sequelize) {


    return sequelize.define('Transaction', {
            UserId: {
                type: DataTypes.STRING
            },
            amount: {
                type: DataTypes.FLOAT
            },
            description: {
                type: DataTypes.STRING
            },
            type: {
                type: DataTypes.STRING
            },
            status: {
                type: DataTypes.STRING
            }
        }
    )


};

