import {DataTypes, Sequelize} from 'sequelize';
import UserModel from './user.js'
import RoleModel from './role.js'
import PlanModel from './plan.js'
import PaymentModel from './payment.js'
import UserPlanModel from './user_plan.js'

import TransactionModel from "./transaction.js";
import WalletModel from "./wallet.js";
import 'dotenv'
import TicketModel from './ticket.js'
import TicketBodyModel from './ticketbody.js'
import DebitModel from './debit.js'
import OtpModel from './otp.js'
import ShowTransActionsModel from './showtransaction.js'
import dbConfig from "../config/data.cjs";


// const sequelize = new Sequelize(configs[process.env.NODE_ENV] || configs['development']);
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    port: dbConfig.port,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const User = UserModel(sequelize)
const Role = RoleModel(sequelize)

const Plan = PlanModel(sequelize)

const Payment = PaymentModel(sequelize)


const User_plan = UserPlanModel(sequelize)

User_plan.belongsTo(User)
User_plan.belongsTo(Plan)


const Transaction = TransactionModel(sequelize)

Transaction.belongsTo(User)

const Wallet = WalletModel(sequelize)

User_plan.belongsTo(Plan)


User.belongsToMany(Role, {through: 'User_Role'});
Role.belongsToMany(User, {through: 'User_Role'});


const User_Role = sequelize.define('User_Role', {}, {timestamps: false});

User.belongsToMany(Role, {through: User_Role});
Role.belongsToMany(User, {through: User_Role});


User.hasOne(Wallet)


const Ticket = TicketModel(sequelize);

Ticket.belongsTo(User)

const TicketBody = TicketBodyModel(sequelize);

Ticket.hasMany(TicketBody)


const Debit = DebitModel(sequelize)

Payment.hasOne(Debit)

const Otp = OtpModel(sequelize)

const ShowTransActions = ShowTransActionsModel(sequelize)


export {sequelize}
export {User}
export {Plan}
export {Role}
export {Payment}
export {User_Role}
export {User_plan}
export {Transaction}
export {Wallet}
export {Ticket}
export {TicketBody}
export {Debit}
export {Otp}
export {ShowTransActions}
