import {DataTypes} from "sequelize";
import bcrypt from "bcrypt";

export default function (sequelize) {


    return sequelize.define('ShowTransActions', {

            ip: {
                type: DataTypes.STRING
            },
            amount: {
                type: DataTypes.FLOAT
            },
            description: {
                type: DataTypes.STRING
            },
            email: {
                type: DataTypes.STRING
            },
        }
    )


};



