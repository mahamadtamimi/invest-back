import {DataTypes} from "sequelize";
import bcrypt from "bcrypt";

export default function (sequelize) {


    return sequelize.define('TicketBodies', {
            TicketId: {
                type: DataTypes.INTEGER
            },
            text: {
                type: DataTypes.STRING
            },
            side: {
                type: DataTypes.INTEGER
            },
            seen : {
                type: DataTypes.BOOLEAN
            }
        }
    )


};

