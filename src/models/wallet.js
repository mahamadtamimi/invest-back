import {DataTypes} from "sequelize";
import bcrypt from "bcrypt";

export default function (sequelize) {


    return sequelize.define('Wallet', {
            UserId: {
                type: DataTypes.INTEGER
            },
            credit: {
                type: DataTypes.FLOAT
            },
        }
    )


};

