import 'dotenv/config'
import express from "express"
import bodyParser from "body-parser"
import multer from "multer"
import {Plan, sequelize, ShowTransActions, Transaction, User_plan, Wallet} from "../models/index.js";
import './auth.js'
import schedule from 'node-schedule';


const upload = multer() // for parsing multipart/form-data


import cors from 'cors'
import {userRoute} from "../routes/Users.route.js";
import {paymentRoute} from "../routes/Payment.route.js";
import {planRoute} from "../routes/Plan.route.js";
import {SeedRoute} from "../routes/Seeder.route.js";
import {jobRoute} from "../routes/Job.route.js";
import {walletRoute} from "../routes/Wallet.route.js";
import moment from "moment";
import {transactionRoute} from "../routes/Transaction.route.js";
import {ticketRoute} from "../routes/Ticket.route.js";
import {authRoute} from "../routes/Auth.route.js";
import {emailRoute} from "../routes/Email.route.js";


process.env.TZ = 'Etc/Universal';

const app = express()


// const {sequelize} = require("./models");
const corsOpts = {
    origin: '*',

    methods: [
        'GET',
        'POST',
    ],

    allowedHeaders: [
        'Content-Type',
        'Authorization'
    ],
};

app.use(cors(corsOpts));
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded()) // for parsing application/x-www-form-

app.use('/api/v1', userRoute)
app.use('/api/v1', paymentRoute)
app.use('/api/v1', planRoute)
app.use('/api/v1', SeedRoute)
app.use('/api/v1', jobRoute)
app.use('/api/v1', walletRoute)
app.use('/api/v1', transactionRoute)
app.use('/api/v1', ticketRoute)
app.use('/api/v1', authRoute)
app.use('/api/v1', emailRoute)



const job = schedule.scheduleJob('1 0 0 * * * ', async function () {

    const userPlan = await User_plan.findAll({include: Plan})

    if (userPlan) {

        userPlan.map(async (item) => {
            if (item.initAmount === 0) return

            const TestToday = new Date();


            const expireDate = new Date(item.expireAt)
            const date1 = new Date(item.startAt);
            const date2 = new Date();

            console.log(date2)
            // return null

            let Expire_In_Time = expireDate.getTime() - date1.getTime();
            let Expire_In_Days = Math.round(Expire_In_Time / (1000 * 3600 * 24));


            let Difference_In_Time = date2.getTime() - date1.getTime();


            let Difference_In_Days = Math.round(Difference_In_Time / (1000 * 60 * 60 * 24) + 1);


            const dailyProfit = (item.Plan.interestRates / item.Plan.dailyCredit)
            const profit = dailyProfit * item.initAmount / 100;

            const partOfPeriod = // Math.floor
                (Difference_In_Days % item.Plan.withdrawalPeriod)

            console.log('--------------------------------------')
            console.log(Difference_In_Days)
            console.log('--------------------------------------')

            console.log(item.Plan.dailyCredit)
            console.log('--------------------------------------')

            ////  is after expire time

            if (Difference_In_Days < item.Plan.dailyCredit) {

                ////////////////
                console.log('in plan')


                if (partOfPeriod === 0 && Difference_In_Days !== 0) {

                    console.log(`profit and send profit to wallet plan ${moment.utc().format('HH::mm::ss')}`)


                    await Transaction.create({
                        UserId: item.UserId,
                        amount: item.profit + profit,
                        description: `release of investment profit ${item.Plan.name}`,
                        type: 'System internal transaction',
                        status: 'complete',
                    })


                    console.log(`record transaction ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)

                    await Wallet.increment({
                        credit:  item.profit + profit
                    }, {
                        where: {
                            UserId: item.UserId
                        }
                    })


                    console.log(`in wallet ${item.profit} : ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: 0}, {
                        where: {
                            id: item.id
                        }
                    });

                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')

                } else {
                    console.log(`just profit plan ${moment.utc().format('HH::mm::ss')}`)


                    await User_plan.update({profit: item.profit + profit}, {
                        where: {
                            id: item.id
                        }
                    });
                    console.log(`plan profit ${profit} : ${moment.utc().format('HH::mm::ss')}`)
                    console.log('-----------------------------------------------------------------------------')
                }


            } else {

                // console.log('****------****')
                console.log(`${item.id}`)


                // Then, we do some calls passing this transaction as an option:


                await Transaction.create({
                    UserId: item.UserId,
                    amount: item.initAmount + item.profit + profit,
                    description: `release of investment profit ${item.Plan.name}`,
                    type: 'System internal transaction',
                    status: 'complete',
                })


                await Wallet.increment({
                    credit: item.initAmount + item.profit + profit
                }, {
                    where: {
                        UserId: item.UserId
                    }
                })




                await User_plan.update({profit: 0, initAmount: 0}, {
                    where: {
                        id: item.id
                    }
                });


                // If the execution reaches this line, no errors were thrown.
                // We commit the transaction.


            }


        })
    }
});


try {
    await sequelize.sync({force: false})
} catch (error) {
    console.log(error)
}
const port = 8080;
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)

})
