import passport from "passport";
import LocalStrategy from 'passport-local';
import passportJWT from 'passport-jwt';
import {createUser, getUser} from "../controllers/Users.controller.js";
import User from "../models/user.js";
import 'dotenv'
import JwtStrategy from "passport-jwt/lib/strategy.js";
import res from "express/lib/response.js";
import { Role} from "../models/index.js";

passport.use('signup', new LocalStrategy({
    passReqToCallback: true
}, async (req, username, password, done) => {
    try {

        console.log(req.body)
        const latestUser = await getUser({username})

        if (latestUser) {

            done(null, {});
        } else {


            const user = await createUser({
                username,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: req.body.password,
            })


            done(null, user)
        }
    } catch (error) {
        done(error)
    }

}))


passport.use('login', new LocalStrategy(async (username, password, done) => {


    try {
        const user = await getUser({username})

        if (!user) {
            return done(null, false, {message: 'Invalid Username Or Password .'});

        } else {
            const hasValidPassword = user.isValidPassword(password)
            console.log('================================')
            console.log(hasValidPassword)
            if (!hasValidPassword) {

                return done(null, false, {message: 'Invalid Username Or Password .'});
            } else {
                return done(null, user);
            }
        }

    } catch (error) {
        done(error)
    }
}))

passport.use('isSuperAdmin', new passportJWT.Strategy({
    secretOrKey: process.env.JWT_SECRET, jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken()
}, async (tokenPayload, done) => {
    try {
        const {id, username} = tokenPayload.user;
        const user = await getUser({username})

        console.log(user.isAdmin())

        if (!user) {
            return done(null, false, {message: 'You must login !'});
        } else {
            if (user.isAdmin()) {

                done(null, user)

            } else {

                return done(null, false, {message: 'permission must login !'});

            }
        }


    } catch (error) {
        done(error)
    }

}))


passport.use('getMe', new passportJWT.Strategy({
    secretOrKey: process.env.JWT_SECRET, jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken()
}, async (tokenPayload, done) => {
    try {
        const {id, username} = tokenPayload.user;
        const user = await getUser({username})


        if (!user) {
            return done(null, false, {message: 'You must login !'});
        } else {
            done(null, user)
        }


    } catch (error) {
        done(error)
    }

}))

