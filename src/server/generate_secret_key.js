import speakeasy from "speakeasy";

export function generate_secret_key() {
    // Generate a secret key
    const secretKey = speakeasy.generateSecret({ length: 20 });

    return secretKey;
}

