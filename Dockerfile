FROM node:20.13.1-alpine3.19

WORKDIR /user/src/app

COPY package*.json .

RUN npm install --legacy-peer-deps --loglevel verbose

COPY . .

EXPOSE 3044

CMD npm start
